﻿namespace app.models
{
    public class BillerSampleBase
    {
        public string Action { get; set; }
        public string BillerId { get; set; }
        public string StatusIndicator { get; set; }
        public string Description { get; set; }
        public string WorkRequestId { get; set; }
        public string EffectiveDate { get; set; }
        public string Errors { get; set; }
    }
}