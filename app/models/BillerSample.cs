﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace app.models
{
    public class BillerSample
    {
        [Key]
        public int Id { get; set; }
        public string Action { get; set; }
        public string BillerId { get; set; }
        public string StatusIndicator { get; set; }
        public string Description { get; set; }
        public string WorkRequestId { get; set; }
        public string EffectiveDate { get; set; }
        public string Errors { get; set; }
    }
}
