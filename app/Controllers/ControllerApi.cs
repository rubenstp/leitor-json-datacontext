﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using app.models;
using System.Text.Json;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using app.Data;
using Microsoft.EntityFrameworkCore;

namespace app.Controllers
{
    
    [ApiController]
    [Route("billerSample")]
    
    public class ApiController : ControllerBase
    {
        

        [HttpGet]     
        [Route("")]
        public async Task<ActionResult<List<BillerSample>>> Get([FromServices] DataContext context)
        {
            var billerSamples = await context.billerSamples.ToListAsync();
            return billerSamples;
        }


        

        [HttpPost]
        [Route("")]
        public async Task<ActionResult<BillerSample>> Post(
            [FromServices] DataContext context,
            [FromBody] BillerSample model)
        {
            if(ModelState.IsValid)
            {
                context.billerSamples.Add(model);
                await context.SaveChangesAsync();
                return model;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
            
        
       
    }
}
